import { createConnection, Connection, ObjectType, EntitySchema } from 'typeorm'
import ormconfig from '../ormconfig'

let connection: Connection | undefined = undefined

async function initConnection(): Promise<Connection> {
  if (!connection) {
    // console.log('create connection')
    connection = await createConnection(ormconfig)
  }
  return connection
}

export async function getConnection(): Promise<Connection> {
  const conn = await initConnection()
  return conn
}

export async function getRepository<Entity>(
  target: ObjectType<Entity> | EntitySchema<Entity> | string
) {
  const conn = await initConnection()
  return conn.getRepository(target)
}

export async function close(): Promise<void> {
  // console.log('close connection')
  const conn = await initConnection()
  await conn.close()
}
